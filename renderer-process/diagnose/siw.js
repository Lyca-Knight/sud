var classname = document.getElementsByClassName("siw");
var path = require('path');
var spawn = require('child_process').spawn;

var siw = function() {
var executablePath = spawn(path.join(process.env.PORTABLE_EXECUTABLE_DIR, 'Programme\\Diagnose-Tools\\siw\\siw.exe'));
spawn(executablePath, function(err, data) {
      if(err){
         console.error(err);
         return;
      }
   
      console.log(data.toString());
  });
  };

for (var i = 0; i < classname.length; i++) {
    classname[i].addEventListener('click', siw, false);
}