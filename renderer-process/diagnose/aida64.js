var classname = document.getElementsByClassName("aida64");
var path = require('path');
var spawn = require('child_process').spawn;

var aida64 = function() {
var executablePath = spawn(path.join(process.env.PORTABLE_EXECUTABLE_DIR, 'Programme\\Diagnose-Tools\\aida64\\aida64.exe'));
spawn(executablePath, function(err, data) {
      if(err){
         console.error(err);
         return;
      }
   
      console.log(data.toString());
  });
  };

for (var i = 0; i < classname.length; i++) {
    classname[i].addEventListener('click', aida64, false);
}