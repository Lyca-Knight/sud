var classname = document.getElementsByClassName("networkscanner");
var path = require('path');
var spawn = require('child_process').spawn;

var networkscanner = function() {
var executablePath = spawn(path.join(process.env.PORTABLE_EXECUTABLE_DIR, 'Programme\\Diagnose-Tools\\networkscanner\\netscan.exe'));
spawn(executablePath, function(err, data) {
      if(err){
         console.error(err);
         return;
      }
   
      console.log(data.toString());
  });
  };

for (var i = 0; i < classname.length; i++) {
    classname[i].addEventListener('click', networkscanner, false);
}