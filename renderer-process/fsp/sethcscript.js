const classname = document.getElementsByClassName("sethcscript");
const BrowserWindow = require('electron').remote.BrowserWindow
const path = require('path');
const spawn = require('child_process').spawn;

var windowOptions = {
  width: 800,
  height: 600,
  frame: false,
  minimizable: false,
  maximizable: false,
  resizable: false,
  title: "Sethc Script"
}

var sethcscript = function () {
  winseth = new BrowserWindow(windowOptions)
  winseth.on('close', function () { win = null })
  winseth.loadURL(path.join('file://', __dirname, '../../index_sethc.html'))
  winseth.show()
};

for (var i = 0; i < classname.length; i++) {
  classname[i].addEventListener('click', sethcscript, false);
}