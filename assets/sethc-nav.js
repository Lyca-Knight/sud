const storage = require('electron-json-storage')
const path = require('path')
const glob = require('glob')
const {BrowserWindow} = require('electron').remote


// Default to the view that was active the last time the app was open
storage.get('activeSectionButtonId', function (err, id) {
  if (err) return console.error(err)

  if (id && id.length) {
    showMainContent()
    const section = document.getElementById(id)
    if (section) section.click()
  } else {
    activateDefaultSection()
    showMainContent ()
  }
})

document.body.addEventListener('click', function (event) {
  if (event.target.dataset.section) {
    handleSectionTrigger(event)
  } else if (event.target.dataset.modal) {
    handleModalTrigger(event)
  } else if (event.target.classList.contains('modal-hide')) {
    hideAllModals()
  }
})

function handleSectionTrigger (event) {
  hideAllSectionsAndDeselectButtons()

  // Highlight clicked button and show view
  event.target.classList.add('is-selected')

  // Display the current section
  const sectionId = event.target.dataset.section + '-section'
  document.getElementById(sectionId).classList.add('is-shown')
}

function activateDefaultSection () {
  document.getElementById('button-sethc').click()
}

function showMainContent () {
  document.querySelector('.js-nav').classList.add('is-shown')
  document.querySelector('.js-contentsethc').classList.add('is-shown')
}

function hideAllSectionsAndDeselectButtons () {
  const sections = document.querySelectorAll('.js-section.is-shown')
  Array.prototype.forEach.call(sections, function (section) {
    section.classList.remove('is-shown')
  })

  const buttons = document.querySelectorAll('.nav-button.is-selected')
  Array.prototype.forEach.call(buttons, function (button) {
    button.classList.remove('is-selected')
  })
}


//function close button
function closebutton() {
  document.getElementById("close").addEventListener("click", (e) => {
                  var window = BrowserWindow.getFocusedWindow();
                  window.close();
              });
};
document.onreadystatechange =  () => {
    if (document.readyState == "complete") {
      closebutton();
    }
};
